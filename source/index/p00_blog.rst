博客资源
====================

* `colah's blog <http://colah.github.io/>`_ 

  - 包括一些**Neural Networks (General)**，**Visualizing Neural Networks**, **Convolutional Neural Networks**

* `Andrej Karpathy blog <http://karpathy.github.io/>`_ 
* `wisvision <http://wisvision.cn/blog/>`_ 

* `RSarXiv <http://rsarxiv.github.io/>`_ 

  - 该博客包含了NLP相关的内容，包括文本摘要系列，每日论文分享（公众号）
