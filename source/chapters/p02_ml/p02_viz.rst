Visualization
==============

- MSR做的低维可视化

    + paper： `Visualizing Large-scale and High-dimensional Data <https://arxiv.org/abs/1602.00370>`_ 
    + 作者主页： `homepage <https://sites.google.com/site/pkujiantang/big-data-visualization>`_ 
    + `reddit <https://www.reddit.com/r/MachineLearning/comments/4hvql7/160200370_visualizing_largescale_and/>`_ 
    + `R Package <https://github.com/elbamos/largevis>`_ 