# Surveys

*(整理自：https://github.com/robertsdionne/neural-network-papers)*

* [Deep Learning](http://rdcu.be/cW4c)
* [Deep Learning in Neural Networks: An Overview](http://arxiv.org/abs/1404.7828)
* [Deep neural networks: a new framework for modelling biological vision and brain information processing](http://biorxiv.org/content/biorxiv/early/2015/10/26/029876.abstract)
* [A Primer on Neural Network Models for Natural Language Processing](http://u.cs.biu.ac.il/~yogo/nnlp.pdf)
* [Natural Language Understanding with Distributed Representation](http://arxiv.org/abs/1511.07916)
