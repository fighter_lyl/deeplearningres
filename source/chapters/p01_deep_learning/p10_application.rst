Application
===============

* `Video2GIF <http://t.cn/RqkCpRT>`_ 

  - `dataset <http://t.cn/RqkCpRl>`_
  - `result <http://t.cn/RqkCpRY>`_  
  - `demo <http://t.cn/R5Mr9VF>`_ 

* `《Teaching Robots to Feel: Emoji & Deep Learning  💭 💕》 <http://t.cn/R5I8Jzo>`_ 

  - 翻译： `可视化对话的未来：神经网络能让emoji聊天更智能  💭 💕》 <http://t.cn/R5M06Xp>`_ 

* `Elasticsearch based Image Search using RGB Signatures <http://t.cn/R5MdcEA>`_ 

  - 使用RGB签名，基于Elasticsearch的图象搜索。

* `《Transfer Style But Not Color》 <http://t.cn/R5I85R6>`_ 

  - 其实方法比想象的简单，搞过图像后期就能想出来，把RGB空间转换成YUV空间做风格学习就可以保持颜色，有人用TensorFlow做了一个实现可以重现结果
  - `Tensorflow 复现 <http://t.cn/R5M4fOR>`_ 

* app2vec: `Smartphone App Categorization for Interest Targeting in Advertising Marketplace, WWW’16 <http://t.cn/R5xkHs1>`_ 