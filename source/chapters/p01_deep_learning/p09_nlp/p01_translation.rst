Translation
==============

Neural Machine Translation
------------------------------------

* `Neural Machine Translation with External Phrase Memory <http://arxiv.org/abs/1606.01792>`_ 

* `Memory-enhanced Decoder for Neural Machine Translation <http://arxiv.org/abs/1606.02003>`_ 
* `Linguistic Input Features Improve Neural Machine Translation <http://arxiv.org/abs/1606.02892>`_ 
* github: `Attention-based encoder-decoder model for neural machine translation <https://github.com/rsennrich/nematus>`_ 