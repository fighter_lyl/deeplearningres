General Paper & Blog
==========================

* `Very Deep Convolutional Networks for Natural Language Processing <http://arxiv.org/abs/1606.01781>`_ 

    - @机器之心synced 提供的中文介绍《Facebook首次将29层深度卷积网络用于自然语言处理，性能与深度成正比》 `link <http://mp.weixin.qq.com/s?__biz=MzA3MzI4MjgzMw==&mid=2650715969&idx=1&sn=77a7f34750731478249f03deec71e92f>`_ 
    - `洪亮劼阅读笔记 <http://t.cn/R5M2BVN>`_ 

* `自然语言处理 <http://blog.csdn.net/malefactor/article/category/716775>`_ 


Sequence Labelling
======================

* `Improving Recurrent Neural Networks For Sequence Labelling <http://arxiv.org/abs/1606.02555>`_ 


Abstract Generation 
=======================

* `Neural Network-Based Abstract Generation for Opinions and Arguments <http://t.cn/R5MAmvD>`_ 

    - 观点(movie reviews)和论点(topic arguments)的摘要生成
* `自动文摘系列博客 <http://t.cn/R54I8os>`_ 


Text Comprehension
======================

* `Gated-Attention Readers for Text Comprehension <http://t.cn/R5JbfEx>`_ 
* 