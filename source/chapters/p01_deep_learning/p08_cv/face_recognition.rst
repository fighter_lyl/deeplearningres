Face Recognition
=============================


General
--------------------------

* `人脸检测研究2015最新进展 <http://wisvision.cn/blog/2015/07/27/%E4%BA%BA%E8%84%B8%E6%A3%80%E6%B5%8B%E7%A0%94%E7%A9%B62015%E6%9C%80%E6%96%B0%E8%BF%9B%E5%B1%95/>`_ 
* `《Decoding Faces from the Brain | Neuroskeptic》by Neuroskeptic <http://t.cn/R5Mr6sJ>`_  

    - `《Reconstructing Perceived and Retrieved Faces from Activity Patterns in Lateral Parietal Cortex》 <http://t.cn/R5Mr6sx>`_ 