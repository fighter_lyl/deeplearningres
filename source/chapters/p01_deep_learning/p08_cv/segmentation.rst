Segmentation Using Neural Network
=================================


Semantic Segmentation
---------------------

- `ENet: A Deep Neural Network Architecture for Real-Time Semantic Segmentation <http://arxiv.org/abs/1606.02147>`_ 

  + 提出了一个实时性的分割网络，其核心模块类似于轻量级的GoogLeNet Inception；在NVIDIA TX1平台上处理480×320图像可以达到21fps，看起来效果略逊于SegNet


Fully Convolutional Neural Network(FCN) 
-----------------------------------------

* `Accurate Text Localization in Natural Image with Cascaded Convolutional Text Network <http://t.cn/RqUdzza>`_ 

  - 包含两个网络：一个全卷积网络FCN对像素分类，以提取字符区域，再用一个FCN估计文本行中心和行高。在ICDAR2011和2013上大幅度超越SOTA.
