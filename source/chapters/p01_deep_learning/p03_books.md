# Books

## General

* [机器学习经典书籍；书单](http://suanfazu.com/t/topic/15)
	* 包含入门、深入、数学相关的书籍

* [Good Books for All Things Data](http://t.cn/R5MdvLU)
	* 数据科学书单

## Deep Learning

* [Deep Learning](http://www.deeplearningbook.org/)
	- *Ian Goodfellow, Yoshua Bengio, and Aaron Courville* 写的教材
* [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/)
	* *Michael Nielsen* 的深度学习入门电子书
