**Convolution Neural Network**
========================================


Paper
--------

* `Laws, Sausages and ConvNets <http://t.cn/R54Bm4x>`_ 

  - 关于ConvNets内核的技术分析：算法，实现和优化。它是关于ConvNets是如何产生的。包括卷积、反向传播、准线性、重叠性、多维性等，感觉好高大上。

* `Render for CNN: Viewpoint Estimation in Images Using CNNs Trained with Rendered 3D Model Views <http://t.cn/R5xs6Iw>`_ 

  - github: http://t.cn/RyaQpDW

* `Understanding Deep Convolutional Networks <http://arxiv.org/abs/1601.04920>`_ 

* `An Analysis of Deep Neural Network Models for Practical Applications <http://arxiv.org/abs/1605.07678>`_ 

* `Systematic evaluation of CNN advances on the ImageNet <http://arxiv.org/abs/1606.02228>`_ 

    - github: https://github.com/ducha-aiki/caffenet-benchmark


Blog
------------

* `Neural Network Architectures <http://linkis.com/github.io/kDnqR>`_ 

    - 总结各种CNN结构，包括最近的一些进展