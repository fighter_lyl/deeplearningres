General
===============

* `《Introduction to machine learning》by Quentin de Laroussilhe <https://docs.google.com/presentation/d/1O6ozzZHHxGzU-McpvEG09hl7K6oQDd2Taw0FOlnxJc8/preview?slide=id.gfc8b39604_0_0>`_ 

  - 机器学习与深度学习介绍

`Deep Learning Trends @ICLR 2016 <http://t.cn/R54CGCA>`_ 

Miscellaneous
=============

Other list
---------------

*(整理自：https://github.com/robertsdionne/neural-network-papers)*

* `DeepLearning University An Annotated Deep Learning Bibliography Memkite <http://memkite.com/deep-learning-bibliography/>`_
* `github.com/memkite/DeepLearningBibliography <https://github.com/memkite/DeepLearningBibliography>`_ 
* `Deep Learning for NLP resources <https://github.com/andrewt3000/DL4NLP>`_ 
* `Reading List  Deep Learning <http://deeplearning.net/reading-list/>`_ 
* `Reading lists for new MILA students <https://docs.google.com/document/d/1IXF3h0RU5zz4ukmTrVKVotPQypChscNGf5k6E25HGvA/edit>`_ 
* `Awesome Recurrent Neural Networks <https://github.com/kjw0612/awesome-rnn>`_ 
* `Awesome Deep Learning <https://github.com/ChristosChristofidis/awesome-deep-learning>`_ 
* `Deep learning Reading List <http://jmozah.github.io/links/>`_ 
* `A curated list of speech and natural language processing resources <https://medium.com/@joshdotai/a-curated-list-of-speech-and-natural-language-processing-resources-4d89f94c032a>`_ 
* `speech language processing <https://github.com/edobashira/speech-language-processing>`_ 
* `CS089/CS189: Deep Learning, Spring 2015 <http://www.cs.dartmouth.edu/~lorenzo/teaching/cs189/readinglist.html>`_ 
  

Math
----------------

* `线性代数的本质 <http://weibo.com/ttarticle/p/show?id=2309403984255918192567>`_ 
* `《Where do I start with learning machine learning math? | Reddit》 <http://t.cn/R5JUNVR>`_ 


无监督学习
====================

* `Navigating the unsupervised learning landscape <http://t.cn/R5J4sgm>`_ 