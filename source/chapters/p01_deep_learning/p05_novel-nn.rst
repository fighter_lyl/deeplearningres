Novel Ideas
==========================


Adversarial Neural Networks
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `Generative Adversarial Networks <http://arxiv.org/abs/1406.2661>`_ 

  - 提出了对抗网络的框架

* `Adversarial Autoencoders <http://arxiv.org/abs/1511.05644v1>`_ 

  - GAN Autoencoder 版
  - `Generative Adversarial Autoencoders in Theano <https://swarbrickjones.wordpress.com/2016/01/24/generative-adversarial-autoencoders-in-theano/>`_ 
  - github:  `code <https://github.com/mikesj-public/dcgan-autoencoder.git>`_ 

* `Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks <http://arxiv.org/abs/1511.06434>`_ 

  - GAN CNN 版

* `Unsupervised and Semi-supervised Learning with Categorical Generative Adversarial Networks <http://arxiv.org/abs/1511.06390>`_ 

* `Domain-Adversarial Training of Neural Networks <http://arxiv.org/abs/1505.07818>`_ 

* `Adversarially Learned Inference <https://ishmaelbelghazi.github.io/ALI/>`_ 

    - paper `link <https://arxiv.org/abs/1606.00704>`_ 
    - 类似的工作 `Adversarial Feature Learning <https://arxiv.org/abs/1605.09782v1>`_ 

        + reddit 讨论： `Generative Adversarial Networks for Text <https://www.reddit.com/r/MachineLearning/comments/40ldq6/generative_adversarial_networks_for_text/>`_ 


Interesting Work
^^^^^^^^^^^^^^^^^^^^^^^^^^^

- `FractalNet: Ultra-Deep Neural Networks without Residuals <https://arxiv.org/abs/1605.07648>`_ 

  + 分形网络，没有residual的极深网络结构
  + `mxnet 讨论 <https://github.com/dmlc/mxnet/issues/2347>`_ 
