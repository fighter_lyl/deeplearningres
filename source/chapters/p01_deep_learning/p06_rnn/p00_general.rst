General Paper
======================

* `论文：面向序列学习的RNN综述 <http://arxiv.org/abs/1506.00019v4>`_ 
* `论文+代码(C++):基于RNN的文档-上下文语言模型(DCLM) <http://arxiv.org/abs/1511.03962>`_ 

  - `code <https://github.com/jiyfeng/dclm>`_ 

* `论文:LSTM-RNN的FPGA实现 <http://arxiv.org/abs/1511.05552v3>`_ 
* `Action Recognition using Visual Attention <http://arxiv.org/abs/1511.04119>`_ 

  - LSTM/RNN 动作识别
  - `code <https://github.com/kracwarlock/action-recognition-visual-attention>`_

* `A Simple Way to Initialize Recurrent Networks of Rectified Linear Units <http://arxiv.org/abs/1511.01432v1>`_ 

  - RNN与Relu初始化方法

* `Sequence to Sequence -- Video to Text <http://arxiv.org/abs/1505.00487v3>`_ 

  - LSTM/RNN 视频自动描述框架
  - `project <http://vsubhashini.github.io/s2vt.html>`_ 
  - `code <https://github.com/vsubhashini/caffe/tree/recurrent/examples/s2vt>`_ 

* `Semi-supervised Sequence Learning <http://arxiv.org/abs/1511.01432>`_ 

  - 论文:(Google)(LSTM/RNN)半监督序列学习

* `A Unified Tagging Solution: Bidirectional LSTM Recurrent Neural Network with Word Embedding <http://arxiv.org/abs/1511.00215v1>`_ 

  - 论文:基于BLSTM-RNN+词嵌入的统一标注(Tagging)解决方案

* `Deep Knowledge Tracing <http://arxiv.org/abs/1506.05908>`_ 

  - (RNN)深度知识追踪
  - `code <https://github.com/chrispiech/DeepKnowledgeTracing>`_ 

* `Phenotyping of Clinical Time Series with LSTM Recurrent Neural Networks <http://arxiv.org/abs/1510.07641>`_ 

  - 深度医学——基于LSTM/RNN的临床时序数据表型(多标签)分类

* `A Recurrent Latent Variable Model for Sequential Data <http://arxiv.org/abs/1506.02216>`_ 

  - 论文:潜随机变量RNN
  - `code(Theano) <https://github.com/jych/nips2015_vrnn>`_ 

* `Training Recurrent Neural Networks by Diffusion <http://arxiv.org/abs/1601.04114v1>`_ 

  - 论文:高效的RNN扩散训练算法[MIT]

* `Zoneout: Regularizing RNNs by Randomly Preserving Hidden Activations <https://arxiv.org/abs/1606.01305>`_ 

  - RNN 正则化技巧，bengio

* `MuFuRU: The Multi-Function Recurrent Unit》D Weissenborn, T Rocktäschel (2016)  <http://t.cn/R5Jv1Fb>`_ 

  - `Tensorflow Implementation of Multi-Function Recurrent Unit <http://t.cn/R5J22ZB>`_ 