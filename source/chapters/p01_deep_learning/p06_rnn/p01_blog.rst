Blog | Tutorial
=========================

* `A Deep Dive into Recurrent Neural Nets <http://nikhilbuduma.com/2015/01/11/a-deep-dive-into-recurrent-neural-networks/>`_ 

  - 深入讨论RNN

* `(知乎)LSTM/RNN入门网络资源汇总 <http://www.zhihu.com/question/29411132>`_ 
* `Anyone Can Learn To Code an LSTM-RNN in Python <http://iamtrask.github.io/2015/11/15/anyone-can-code-lstm/>`_ 

  - 人人都能写RNN

* `Long short-term memory(LSTM)数学推导 <http://www.cnblogs.com/changzhiluo/articles/4549679.html>`_ 
* `循环神经网络介绍 <http://blog.csdn.net/heyongluoyao8/article/details/48636251>`_ 
* `Understanding LSTM Networks <http://colah.github.io/posts/2015-08-Understanding-LSTMs/>`_ 

  - 翻译： `理解 LSTM 网络 <http://www.jianshu.com/p/9dc9f41f0b29>`_ 

* `The Unreasonable Effectiveness of Recurrent Neural Networks <http://karpathy.github.io/2015/05/21/rnn-effectiveness/>`_ 

  - 介绍RNN的分类以及一些应用

* `RNN Backprop Through Time Equations <http://t.cn/R5I8zBR>`_ 