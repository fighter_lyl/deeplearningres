应用与实践、经验等
==============================


* `基于RNN LSTM的汪峰老师作词机测试 <http://weibo.com/1770891687/CleJwruLd?type=comment>`_ 

  - `char-rnn <https://github.com/karpathy/char-rnn>`_ 

* `(Torch)用RNN破解CAPTCHAS验证码(96%) <https://github.com/arunpatala/captcha>`_ 
* `RNN雨情预测(Kaggle's "How Much Did It Rain?II"第一名方案&代码) <http://simaaron.github.io/Estimating-rainfall-from-weather-radar-readings-using-recurrent-neural-networks/>`_ 

  - `code <https://github.com/simaaron/kaggle-Rain>`_ 
