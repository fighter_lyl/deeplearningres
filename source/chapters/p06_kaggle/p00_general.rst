General
=======

Technique 
----------

- Adversarial validation

  + `part 1 <http://fastml.com/adversarial-validation-part-one/>`_
  + `part 2 <http://fastml.com/adversarial-validation-part-two/>`_

- `Kaggle: Journey to #1 - It’s not the destination…it’s the journey! <http://t.cn/R5JMq0e>`_ \
- `Higgs Boson Machine Learning Challenge <http://t.cn/R5J4ECX>`_ 