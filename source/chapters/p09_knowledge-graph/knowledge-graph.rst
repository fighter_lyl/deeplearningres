Knowledge Graph
=====================


General
-----------------------

- `知识图谱——机器大脑中的知识库 <http://www.36dsj.com/archives/31317>`_  By 刘知远
- `Bayesian Poisson Tucker Decomposition (BPTD) for Learning the Structure of International Relations <https://arxiv.org/abs/1606.01855>`_ 

    + 很有意思，依据国家之间的交互事件数据（“country i took action a toward country j at time t.”）发掘 国家(如Venezuela) - 团体(OPEC, LAIA)成员 关系等