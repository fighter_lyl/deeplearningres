ICML
==================
(International Conference On Machine Learning)

* `"ICML 2016 Workshop on Data-Efficient Machine Learning - Accepted Papers" <http://t.cn/R5wvAXG>`_ 

  - ` @机器之心synced 提供的中文介绍《ICML 2016｜「Data-Efficient 机器学习」研讨会已接收论文概述：如何让机器学习更高效的利用数据》 <http://t.cn/R5JA9AB>`_

* `ICML 16 论文集 <http://t.cn/R5J4STt>`_  