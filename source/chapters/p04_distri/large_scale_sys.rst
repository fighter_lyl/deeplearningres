Large Scale Systems
==========================

General
---------------

* `Introducing Stream Windows in Apache Flink <http://flink.apache.org/news/2015/12/04/Introducing-windows.html>`_ 

Courses
---------------

- `6.S897: Large-Scale Systems <http://people.csail.mit.edu/matei/courses/2015/6.S897/>`_ 