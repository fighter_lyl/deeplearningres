Spark 
======

Introduction
--------------

* *Big Data Processing with Apache Spark*

  - `Part 1: Introduction <https://www.infoq.com/articles/apache-spark-introduction>`_ 
  - `Part 2: Spark SQL <https://www.infoq.com/articles/apache-spark-sql>`_ 
  - `Part 3: Spark Streaming <http://www.infoq.com/articles/apache-spark-streaming>`_ 
  - `Part 4: Spark Machine Learning <https://www.infoq.com/articles/apache-spark-machine-learning>`_ 

* `Intro to Apache Spark Training <https://www.jianguoyun.com/p/DWX3CwsQvJP7BRiu5hU>`_ 

  - 视频： `part 1 <https://www.youtube.com/watch?v=VWeWViFCzzg>`_ 
  - 视频： `part 2 <https://www.youtube.com/watch?v=KsBXyVL2CHk>`_ 
  - 视频： `part 3 <https://www.youtube.com/watch?v=Cxv_ERVKYE4>`_
  - 视频： `part 4 <https://www.youtube.com/watch?v=U0Lj-hwHqvU>`_  



Machine Learning & Spark
------------------------

- `ALS 在 Spark MLlib 中的实现 <http://dataunion.org/24504.html>`_
-  