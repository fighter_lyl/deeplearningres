Courses
=======

Deep Learning & Neural Network
------------------------------

* `CS231n: Convolutional Neural Networks for Visual Recognition <http://cs231n.stanford.edu/>`_ 

  - 2015年课程的相关资料

* `CS231n: Convolutional Neural Networks for Visual Recognition <http://vision.stanford.edu/teaching/cs231n/index.html>`_ 

  - 2016新课

* `Deep learning at Oxford 2015 <http://www.cs.ox.ac.uk/teaching/courses/2014-2015/ml/>`_ 

  - 牛津大学Nando de Freitas主讲的机器学习课程，重点介绍深度学习，还请来Deepmind的Alex Graves和Karol Gregor客座报告，内容、讲解都属一流
  - `Youtube playLists <https://www.youtube.com/playlist?list=PLE6Wd9FR--EfW8dtjAuPoTuPcqmOV53Fu>`_ 
  - 已存至百度盘

* `Neural networks class - Université de Sherbrooke <https://www.youtube.com/playlist?list=PL6Xpj9I5qXYEcOhn7TqghAJ6NAPrNmUBH>`_ 

  - 超棒的神经网络课程，深入浅出介绍深度学习，由Hugo Larochelle（Yoshua Bengio的博士生，Geoffrey Hinton之前的博士后）主讲
  - `课程讲义+参考资料 <http://info.usherbrooke.ca/hlarochelle/neural_networks/content.html>`_ 
  - `Google Groups <https://groups.google.com/forum/#!forum/neural-networks-online-course>`_ 

* `Neural Networks for Machine Learning <https://class.coursera.org/neuralnets-2012-001/lecture>`_ 

  - Geoffrey Hinton‘s Courses

* `MOOC:Deep Learning - Taking machine learning to the next level <https://www.udacity.com/course/deep-learning--ud730>`_ 

  - [Google]基于TensorFlow的深度学习/机器学习课程

Machine Learning
----------------

* `【课程讲义:在线学习】《Lecture Notes on Online Learning》 <http://www-stat.wharton.upenn.edu/~rakhlin/courses/stat991/papers/lecture_notes.pdf>`_ 

  - 【课程讲义:在线学习】《Lecture Notes on Online Learning》by Alexander Rakhlin [UC Berkeley]  (2009)

* `统计机器学习 <http://study.163.com/course/introduction.htm?courseId=1692004#/courseDetail>`_ 

  - 统计学习是关于计算机基于数据构建的概率统计模型并运用模型对数据进行预测和分析的一门科学，统计学习也称为统计机器学习。本课程主讲老师为上海交通大学计算机科学与工程系张志华教授

* 机器学习基石 & 机器学习技法 

  - youtube 上有

* `SML: Scalable Machine Learning <http://t.cn/zOIZCzl>`_

  -  slides: http://t.cn/R5xkfG0



Nature Language Processing
--------------------------

* `CS224d: Deep Learning for Natural Language Processing <http://cs224d.stanford.edu/>`_ 

  - 斯坦福自然语言处理

* `课程资料:(Chris Manning)自然语言处理 <http://web.stanford.edu/class/cs224n/>`_ 

  - CS 224N / Ling 284  —  Natural Language Processing》by Chris Manning [Stanford]
  - `Calendar&Syllabus <http://web.stanford.edu/class/cs224n/syllabus.shtml>`_ 
  - `Final Projects 2015-2016 <http://nlp.stanford.edu/courses/cs224n/2015/>`_ 
  - `older Final Projects <http://nlp.stanford.edu/courses/cs224n/>`_ 

Math
----

* `视频+资料:CMU凸优化课程(Ryan Tibshirani) <http://www.stat.cmu.edu/~ryantibs/convexopt/>`_ 

  - CMU 凸优化课程
  - `Youtube <https://www.youtube.com/playlist?list=PLjbUi5mgii6BZBhJ9nW7eydgycyCOYeZ6>`_ 
  - 已存至网盘

* `哈佛大学蒙特卡洛方法与随机优化课程 <http://cm.dce.harvard.edu/2014/02/24104/L24/materials/slideFrame001.html>`_ 

  - 哈佛应用数学研究生课程，由V Kaynig-Fittkau、P Protopapas主讲，Python程序示例，对贝叶斯推理感兴趣的朋友一定要看看，提供授课视频及课上IPN讲义