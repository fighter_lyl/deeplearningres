Bayesian Machine Learning
==================================


Awesome Lists
-----------------------

- `Bayesian Machine Learning <https://github.com/ReactiveCJ/BayesianLearning>`_ 
  


General
-------
- `Bayesian Machine Learning <http://fastml.com/bayesian-machine-learning/>`_ 

    + 介绍了贝叶斯机器学习中的一些主要概念，从宏观上进行了介绍



Bayesian Inference
-------------------------

- `Alleviating Uncertainty in Bayesian Inference with MCMC sampling and Metropolis-Hastings <http://www.exaptive.com/blog/alleviating-uncertainty-in-bayesian-inference-with-mcmc-sampling-and-metropolis-hastings>`_ 