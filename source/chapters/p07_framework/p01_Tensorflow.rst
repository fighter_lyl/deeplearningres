Tensorflow
==============

Wrap with Tensorflow
----------------------------

- `EasyTensorflow <https://github.com/calvinschmdt/EasyTensorflow>`_ 

  + This package provides users with methods for the automated building, training, and testing of complex neural networks using Google's Tensorflow module. The project includes objects that perform both regression and classification tasks.


Tutorial & Example
----------------------

* `Dynamic_RNN_Tensorflow <https://github.com/KnHuq/Dynamic_RNN_Tensorflow>`_ 

    - Dynamic Vanilla RNN, GRU, LSTM,2layer Stacked LSTM with Tensorflow Higher Order Ops

* `TensorFlow VGG-16 pre-trained model <http://t.cn/R5J29I1>`_ 
* `ResNet model in TensorFlow <http://t.cn/RGrqfHg>`_ 
* `Tensorflow Implementation of Multi-Function Recurrent Unit <http://t.cn/R5J22ZB>`_ 
* 